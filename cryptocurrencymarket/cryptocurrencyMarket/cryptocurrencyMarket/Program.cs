﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cryptocurrencyMarket
{
    public class Program
    {
        static void viewBalance(Currency[] allSubaccounts)
        {
            Console.WriteLine("Summary:");
            decimal totalValue = 0m;
            foreach (Currency c in allSubaccounts)
            {
                Console.WriteLine("{0}: {1} ({2:c})", c.Name, c.CurrentBalance, c.CurrentValue);
                totalValue = totalValue + c.CurrentValue;
            }
            Console.WriteLine("Total Cash Equivalent Balance:\t{0:c}\n\n", totalValue);
        }

        static Currency[] transactCryptocurrency(Currency fromAcct, Currency toAcct)
        {
            Console.WriteLine("You can purchase up to {0:c} of {1}\n\nHow much would you like?", fromAcct.CurrentValue, toAcct.Name);
            //Chris TODO: need to ensure 1. they put in a decimal and 2. the amount does not exceed the value in their fromaccount
            decimal purchaseAmount = Decimal.Parse(Console.ReadLine());
            fromAcct.CurrentBalance = fromAcct.CurrentBalance - (purchaseAmount / fromAcct.Value);
            toAcct.CurrentBalance = toAcct.CurrentBalance + (purchaseAmount / toAcct.Value);
            Currency[] returnArray = { fromAcct, toAcct };
            viewBalance(returnArray);
            return returnArray;
        }

        public static void Main(string[] args)
        {
            Currency bitcoinBalance = new Currency().Bitcoin(0);
            Currency etheriumBalance = new Currency().Etherium(0);
            Currency litecoinBalance = new Currency().Litecoin(0);
            Currency cashBalance = new Currency().Cash(5000);
            Currency fromAccount = new Currency();
            Currency toAccount = new Currency();
            Currency[] allAccounts = { cashBalance, bitcoinBalance, etheriumBalance, litecoinBalance };
            string userSelected;
            bool repeat = true;


            Console.WriteLine("Welcome to the Cryptocurrency Market!");

            do
            {
                Console.WriteLine("Please select from the following menu options:\n\t1) (V)iew Balance\n\t2) (T)ransaction\n\t");
                userSelected = Console.ReadLine().ToUpper();

                switch (userSelected)
                {
                    case "1":
                    case "V":
                    case "VIEW BALANCE":
                        Console.WriteLine("**********************************************\n");
                        viewBalance(allAccounts);
                        Console.WriteLine("**********************************************\n");
                        break;

                    case "2":
                    case "T":
                    case "TRANSACTION":
                        do
                        {
                            do
                            {

                                Console.WriteLine("FROM which account would you like to make a purchase? (Cash, Bitcoin, Etherium, Litecoin)\n\nAccount Information\n");
                                viewBalance(allAccounts);
                                userSelected = Console.ReadLine().ToUpper();
                                foreach (Currency c in allAccounts)
                                {
                                    if (c.Name.ToUpper() == userSelected)
                                    {
                                        fromAccount = c;
                                    }
                                    //Mike TODO: Validate that the selected account ACTUALLY corresponds to an existing account
                                    //AND that it has moeny in it
                                    //if it doesn't, they'll need to be sent back to account selection
                                    //i.e. if it IS a valid selection, set repeat = false;
                                }
                            } while (repeat);
                            //Set repeat back to true for consistency in logic
                            repeat = true;
                            do
                            {
                                Console.WriteLine("You have selected {0} which has a cash balance of {1:c}.\n\n" +
                                    "Into which account would you like to purchase? (Cash, Bitcoin, Etherium, Litecoin)", fromAccount.Name, fromAccount.CurrentValue);
                                userSelected = Console.ReadLine().ToUpper();

                                foreach (Currency c in allAccounts)
                                {
                                    if (c.Name.ToUpper() == userSelected)
                                    {
                                        toAccount = c;
                                    }
                                    //Mike TODO: Validate that the selected account ACTUALLY corresponds to an existing account
                                    //AND that it's different from the FROM account
                                    //If it passes this validation, set repeat = false;
                                }
                            } while (repeat);
                        } while (repeat);
                        Currency[] transactionResult = transactCryptocurrency(fromAccount, toAccount);
                        //this ensures the user gets back to the main menu
                        repeat = true;
                        break;

                    default:
                        Console.WriteLine("Please select a valid option from the menu.\n\n");
                        break;

                }


            } while (repeat);
        }
    }
}
