﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cryptocurrencyMarket
{
    public class Currency
    {
        public string Name { get; set; }
        public decimal Value { get; set; }
        public decimal CurrentBalance { get; set; }
        public Currency() { }
        public Currency(string name, decimal value)
        {
            Name = name;
            Value = value;
        }
        public Currency(string name, decimal value, decimal balance)
        {
            Name = name;
            Value = value;
            CurrentBalance = balance;
        }

        public decimal CurrentValue
        {
            get
            {
                return Value * CurrentBalance;
            }

        }
        public Currency convertCurrency(Currency amountToConvert, Currency fromCurrency, Currency toCurrency)
        {
            decimal convertedValue = (amountToConvert.CurrentBalance * fromCurrency.Value) / toCurrency.Value;
            Currency newAmount = new Currency(toCurrency.Name, toCurrency.Value, convertedValue);
            return newAmount;
        }

        public void updateBalance(decimal amountRequested, Currency sellFromAccount, Currency purchaseToAccount)
        {

        }

        public Currency Bitcoin(decimal balance)
        {
            Currency bitcoin = new Currency("Bitcoin", 9694.02m, balance);
            return bitcoin;
        }

        public Currency Etherium(decimal balance)
        {
            Currency etherium = new Currency("Etherium", 479.76m, balance);
            return etherium;
        }

        public Currency Litecoin(decimal balance)
        {
            Currency litecoin = new Currency("Litecoin", 90.67m, balance);
            return litecoin;
        }

        public Currency Cash(decimal balance = 0)
        {
            Currency cash = new Currency("Cash", 1m, balance);
            return cash;
        }


    }
}
